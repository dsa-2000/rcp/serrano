# 🌶️ SERRANO: ScalablE Radio cameRA pipeliNe tOolkit

A [Legate](https://nv-legate.github.io/legate.core/) library for the
construction of radio camera pipeline and aperture synthesis imaging
applications.

Largely based on previous work in
[symcam](https://dsa-2000/rcp/symcam), but using a new design and
implementation of task and function interfaces. Another major
difference between the projects is that this project implements a
toolkit for the construction of the radio camera application, and does
not provide such an implementation itself.

## Spack package

[Spack](https://spack.io) package is available in the DSA-2000 Spack
[package repository](https://gitlab.com/dsa-2000/code/spack).

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/rcp/serrano. Instructions for cloning the
code repository can be found on that site. To provide feedback (*e.g*
bug reports or feature requests), please see the issue tracker on that
site. If you wish to contribute to this project, please read
[CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.
